from django.contrib import admin
from Test.models import Test, TipoPregunta, Pregunta, Respuesta, PreguntaRespuesta, TestPregunta, UsuarioTest
# Register your models here.

admin.site.register(Test)
admin.site.register(TipoPregunta)
admin.site.register(Pregunta)
admin.site.register(Respuesta)
admin.site.register(PreguntaRespuesta)
admin.site.register(TestPregunta)
admin.site.register(UsuarioTest)



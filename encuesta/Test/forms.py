from django import forms
from .models import Genero, Usuario, UsuarioTest
import datetime

estado = (
    (1, "Activo"),
    (0, "Inactivo"),
)

class RegistroForm(forms.Form):
    nombre = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nombre Completo', 'size': 20, 'required': True}))
    correo = forms.EmailField(label = 'Correo', widget=forms.TextInput(attrs={'placeholder': 'Correo Electronico', 'size': 0, }))
    edad = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Edad', 'size': 20, 'required': True}))
    genero = forms.ModelChoiceField(empty_label="Seleccione Genero", label='Genero', queryset= Genero.objects.all())

    def clean(self):
        return self.cleaned_data

class EditForm(forms.Form):
    nombre = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nombre Completo', 'size': 20, 'required': True}))
    correo = forms.EmailField(label = 'Correo', widget=forms.TextInput(attrs={'placeholder': 'Correo Electronico', 'size': 0, }))
    edad = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Edad', 'size': 20, 'required': True}))
    genero = forms.ModelChoiceField(empty_label="Seleccione Genero", label='Genero', queryset= Genero.objects.all())
    estado = forms.ChoiceField(choices=estado, widget=forms.Select(attrs={}))

    def clean(self):
        return self.cleaned_data

class PickUserForm(forms.Form):
    usuario = forms.ModelChoiceField(empty_label="Seleccione Usuario", label='', queryset= Usuario.objects.exclude(id_usuario__in=UsuarioTest.objects.values_list('id_usuario', flat=True)).exclude(estado = 0))

    def clean(self):
        return self.cleaned_data
# Generated by Django 2.1.7 on 2019-09-13 09:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Test', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pregunta',
            fields=[
                ('id_pregunta', models.IntegerField(primary_key=True, serialize=False)),
                ('pregunta', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'Pregunta',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PreguntaRespuesta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('correcta', models.IntegerField()),
            ],
            options={
                'db_table': 'Pregunta_Respuesta',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Respuesta',
            fields=[
                ('id_respuesta', models.IntegerField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'Respuesta',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id_test', models.IntegerField(primary_key=True, serialize=False)),
                ('titulo', models.CharField(max_length=50)),
                ('descripcion', models.CharField(max_length=250)),
            ],
            options={
                'db_table': 'Test',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TestPregunta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'db_table': 'Test_Pregunta',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TipoPregunta',
            fields=[
                ('id_tipo', models.IntegerField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'Tipo_Pregunta',
                'managed': False,
            },
        ),
    ]

from django.db import models

# Create your models here.
class Genero(models.Model):
    id_genero = models.IntegerField(primary_key=True)
    genero = models.CharField(max_length=15)

    def __str__(self):
        return self.genero

    class Meta:
        managed = False
        db_table = 'Genero'

class Usuario(models.Model):
    id_usuario = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=255)
    correo = models.CharField(max_length=255)
    edad = models.IntegerField()
    estado = models.TextField()  # This field type is a guess.
    id_genero = models.ForeignKey('Genero', models.DO_NOTHING, db_column='id_genero')
    fecha_creacion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        managed = False
        db_table = 'Usuario'

class Test(models.Model):
    id_test = models.IntegerField(primary_key=True)
    titulo = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=250)

    def __str__(self):
        return self.titulo

    class Meta:
        managed = False
        db_table = 'Test'

class TipoPregunta(models.Model):
    id_tipo = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre
    
    class Meta:
        managed = False
        db_table = 'Tipo_Pregunta'

class Pregunta(models.Model):
    id_pregunta = models.IntegerField(primary_key=True)
    pregunta = models.CharField(max_length=50)
    id_tipo = models.ForeignKey('TipoPregunta', models.DO_NOTHING, db_column='id_tipo')

    def __str__(self):
        return self.pregunta

    class Meta:
        managed = False
        db_table = 'Pregunta'

class TestPregunta(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    id_test = models.ForeignKey('Test', models.DO_NOTHING, db_column='id_test')
    id_pregunta = models.ForeignKey('Pregunta', models.DO_NOTHING, db_column='id_pregunta')

    class Meta:
        managed = False
        db_table = 'Test_Pregunta'

class Respuesta(models.Model):
    id_respuesta = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=50)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'Respuesta'

class PreguntaRespuesta(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    id_pregunta = models.ForeignKey('Pregunta', models.DO_NOTHING, db_column='id_pregunta')
    id_respuesta = models.ForeignKey('Respuesta', models.DO_NOTHING, db_column='id_respuesta')
    correcta = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'Pregunta_Respuesta'

class UsuarioTest(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    id_test = models.ForeignKey('Test', models.DO_NOTHING, db_column='id_test')
    id_usuario = models.ForeignKey('Usuario', models.DO_NOTHING, db_column='id_usuario')
    nota = models.IntegerField()
    fecha_realizacion = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'Usuario_Test'
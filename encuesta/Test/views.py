from django.shortcuts import render, redirect
from .models import Usuario, TestPregunta, Respuesta, PreguntaRespuesta, Test, UsuarioTest
from .forms import RegistroForm, PickUserForm, EditForm
import io
from django.http import FileResponse, HttpResponse
from django.db.models import Avg

import reportlab
from reportlab.pdfgen import canvas
from reportlab.graphics import renderPDF
from reportlab.graphics.shapes import Drawing
from reportlab.lib.pagesizes import A4
from reportlab.graphics.charts.lineplots import LinePlot
from reportlab.graphics.charts.barcharts import VerticalBarChart
from reportlab.graphics.widgets.markers import makeMarker
from reportlab.lib.colors import PCMYKColor

from datetime import datetime
# Create your views here.

def index(request):
    for key in list(request.session.keys()):
        del request.session[key]
    return render(request, "index.html")

def users(request):
    if request.method == 'POST':
        form = RegistroForm(request.POST or None)
        if form.is_valid():
            now = datetime.now()
            nuevo = Usuario()
            nuevo.nombre = form.cleaned_data['nombre']
            nuevo.correo = form.cleaned_data['correo']
            nuevo.edad = form.cleaned_data['edad']
            ##gen = form.cleaned_data['genero']
            nuevo.id_genero = form.cleaned_data['genero']
            nuevo.estado = 1
            nuevo.fecha_creacion = now.strftime('%Y-%m-%d')
            nuevo.save()
            usuarios = Usuario.objects.all()
            form = RegistroForm()
            context = {
                "usuarios": usuarios,
                "form":form
            }
        return render(request, "users.html", context)
    else:
        usuarios = Usuario.objects.all()
        form = RegistroForm(request.POST or None)
        context = {
            "usuarios": usuarios,
            "form": form
            }
        return render(request, "users.html", context)

def delete_user(request, user_id):
    usuario = Usuario.objects.get(pk = user_id)
    usuario.estado = 0
    usuario.save()
    return redirect('users')

def edit_user(request, user_id):
    if request.method == 'POST':
        usuario = Usuario.objects.get(pk = user_id)
        form = EditForm(request.POST or None)
        if form.is_valid():
            now = datetime.now()
            usuario.nombre = form.cleaned_data['nombre']
            usuario.correo = form.cleaned_data['correo']
            usuario.id_genero = form.cleaned_data['genero']
            usuario.estado = form.cleaned_data['estado']
            usuario.edad = form.cleaned_data['edad']
            usuario.fecha_modificacion = now.strftime('%Y-%m-%d')
            usuario.save()
            return redirect('users')
        else:
            return redirect('index')
    else:
        usuario = Usuario.objects.get(pk = user_id)
        form = EditForm()
        form.initial = {'nombre': usuario.nombre,
                        'correo': usuario.correo,
                        'edad': usuario.edad,
                        'genero': usuario.id_genero,
                        'estado': usuario.estado
                        }
        context = {
            "form":form
        }
        return render(request, "edit.html", context)

def select_user(request):
    if request.method == 'POST':
        form = PickUserForm(request.POST or None)
        if form.is_valid():
            request.session['usuario'] = form.cleaned_data['usuario'].id_usuario
            return redirect('do_test')
        else:
            return redirect('index')
    else:
        form = PickUserForm()
        context = {
            "form" : form,
        }
        return render(request, "select.html", context)

def do_test(request):
    if len(list(request.session.keys())) > 0:
        if request.method == 'POST':
            preguntas = TestPregunta.objects.all().filter(id_test = 1)
            cont = 0
            for p in preguntas:
                if p.id_pregunta.id_tipo.id_tipo == 1: #OpcionMultiple
                    val = request.POST.get(str(p.id_pregunta.id_pregunta))
                    resp = PreguntaRespuesta.objects.get(id_pregunta = p.id_pregunta.id_pregunta, id_respuesta = val)
                    if(resp.correcta == 1):
                        cont = cont + 1
                elif p.id_pregunta.id_tipo.id_tipo == 2: #V/F
                    val = request.POST.get(str(p.id_pregunta.id_pregunta))
                    resp = PreguntaRespuesta.objects.get(id_pregunta = p.id_pregunta.id_pregunta, id_respuesta = val)
                    if(resp.correcta == 1):
                        cont = cont + 1
                else: #Resp Corta
                    #print(p.id_pregunta.id_pregunta)
                    correcta = PreguntaRespuesta.objects.get(id_pregunta = p.id_pregunta.id_pregunta)
                    val = request.POST.get(str(p.id_pregunta.id_pregunta))
                    if str(val) == str(correcta.id_respuesta) :
                        cont = cont + 1
            request.session['nota'] = cont
            now = datetime.now()
            res = UsuarioTest()
            res.id_test = Test.objects.get(id_test = 1)
            res.id_usuario = Usuario.objects.get(id_usuario = request.session.get('usuario') )
            res.nota = cont
            res.fecha_realizacion = now.strftime('%Y-%m-%d')
            res.save()
            return redirect('result')
        else:
            examen = Test.objects.get(id_test = 1)
            preguntas = TestPregunta.objects.all().filter(id_test = 1)
            respuestas = PreguntaRespuesta.objects.all()
            usuario = Usuario.objects.get(id_usuario = request.session.get('usuario') )
            context = {
                "preguntas": preguntas,
                "respuestas": respuestas,
                "examen":examen,
                "usuario": usuario,
            }
            return render(request, "test.html", context)
    else:
        return redirect('index')

def result(request):
    usuario = Usuario.objects.get(id_usuario = request.session.get('usuario') )
    nota = request.session.get('nota')
    context = {
        "usuario" : usuario,
        "nota": nota,
    }
    return render(request, "results.html", context)

def generate(request):
    media = UsuarioTest.objects.aggregate(Avg('nota'))
    nota = request.session.get('nota')
    mensaje = ""
    d = Drawing(500, 500)
    bar = VerticalBarChart()
    bar.x = 100
    bar.y = 125
    data = []
    lista = []
    resultados = UsuarioTest.objects.all()
    for resultado in resultados:
        data.append(resultado.nota)
        lista.append(resultado.id_usuario.nombre)

    ##bar.data = data
    #bar.data = [[1],[2]]
    #bar.categoryAxis.categoryNames = ['Usuario1', 'Usuario2']
    bar.data = []
    bar.data.append(data)
    bar.categoryAxis.categoryNames = lista

    bar.width = 300
    bar.height = 250
    bar.valueAxis.rangeRound = 'both'
    bar.valueAxis.valueMax = 10
    bar.valueAxis.forceZero = 1
    bar.bars[0].fillColor   = PCMYKColor(0,100,100,40,alpha=85)
    bar.bars[1].fillColor   = PCMYKColor(23,51,0,4,alpha=85)
 
    d.add(bar, '')


    # Create a file-like buffer to receive PDF data.
    buffer = io.BytesIO()

    # Create the PDF object, using the buffer as its "file."
    p = canvas.Canvas(buffer, pagesize=A4)
    p.setTitle("Comparacion de Resultados")
    renderPDF.draw(d, p, 50, 0)
    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    #p.drawString(100, 100, "Hello world.")
    # Close the PDF object cleanly, and we're done.
    usuario = Usuario.objects.get(id_usuario = request.session.get('usuario') )
    p.setFont('Helvetica', 14)
    p.drawString(75,700, "Bienvenido: ")
    p.setFont('Helvetica-Bold', 14)
    p.drawString(150,700, usuario.nombre)
    now = datetime.now()
    p.drawString(450,700, now.strftime('%Y-%m-%d'))
    p.setFont('Helvetica', 12)
    p.drawString(75,650, "A continuacion se muestra una grafica de linea con los resultados de otros participantes")
    p.drawString(75,600, "En base a esto usted podra comparar su desempeño con el de los demas participantes")

    if nota > media['nota__avg']:
        mensaje = "por lo que su nota se encuentra por encima de la media."
    elif nota < media['nota__avg']:
        mensaje = "por lo que su nota se encuentra por debajo de la media."
    else:
        mensaje = "por lo que su nota se es igual a la media."

    p.drawString(75,550, "Su nota fue de: "+ str(nota) + " " + mensaje )
    p.drawString(75,500, "La media en esta prueba es de: "+ str(media['nota__avg']) )
    p.showPage()
    p.save()

    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename='hello.pdf')